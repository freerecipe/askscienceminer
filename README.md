# README #

Welcome to the AskScienceMiner project on bitbucket

### About AskScienceMiner ###

AskScienceMiner is a python program that does three main tasks:

* build a categorized corpus from the submissions
to reddit.com/r/askscience

* use this corpus to train a classifier and classify posts to askreddit

* evaluate the performance of different classifiers and classification parameters

### Running AskScienceMiner ###

**The following depencencies are needed to run AskScienceMiner:**

* [NLTK](http://www.nltk.org/)
* [PRAW](https://praw.readthedocs.org/en/stable/)
* [sklearn](http://scikit-learn.org/stable/install.html)
* [matplotlib](http://matplotlib.org/)

**Running AskScienceminer:**

Use a python console for running the program.

First import the evaluation module, which the starting point for evaluating and understanding AskScienceMiner:

```
#!python
import evaluation
```

Then run

```
#!python
evaluation.full_run()
```

to do a full run in order to test complete functionality of corpus building, classification
and evaluation.


You can also run

```
#!python
evaluation.fast_run()
```
for a run with cached data, which is faster. (see source code and documentation files for details on this)

In order to see how the results discussed in the report were obtained run:

```
#!python
evaluation.report_run1()
```

and 

```
#!python
evaluation.report_run2()
```

Each run will produce one matplotlib plot showing the evaluation of several methods of document classification.
The corpus is located in the corpusxml folder.


### Contact ###

write an e-mail to bertio('at')gmx.de to contact the author of the program