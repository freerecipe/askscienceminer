<submission>
  <fullname name="t3_3eedt2"/>
  <category name="computing"/>
  <title>To what computational structure are regex replace sequences Turing-equivalent?</title>
  <selftext>To clarify:

Create a finite sequence of regular expression replacement statements in some language that allows for them. For the sake of rigor, let's say that we're using the ECMAScript 5 implementation of regular expressions, we're allowed to use any feature included (including non-regular extensions such as back-references), and every statement is of the form:

    str = str.replace(/some_regex/g, "some_replacement");

In particular, note that all regular expressions are global, and we always write back to the same string. Input is the initial value of str, and output is the final value of str. (If a boolean output is desired for mathematical purposes, let the output be whether or not this final value is nonempty.) There are no control structures.

Is there another well-known computational abstraction/machine (e.g. pushdown automata) to which this class of algorithms is Turing-equivalent? It's not Turing-complete, because every algorithm always halts (thus halting is decidable, trivially). At the same time, the non-regular extensions guarantee that it's more powerful than a finite automaton.

If it helps, I'm fairly certain that this class of algorithms is Turing-complete if we allow looping (perhaps by putting the whole algorithm into a loop and adding a way to signal termination), as then one could simply represent every state of the machine as a special character sequence (adding characters to the language) and create a replacement for each rule. Thoughts?

(If this is too tricky to answer in a single response, please advise as to a better subreddit.)</selftext>
</submission>
