<submission>
  <fullname name="t3_3ezqc3"/>
  <category name="physics"/>
  <title>When it comes to semi-reflective materials, why do physicists say that photon reflection is dictated by probability?</title>
  <selftext>I have seen this mentioned and explained multiple times, including by Richard Feynman. They (generally) say that when you shoot photons at a semi-reflective surface (such as a half-silvered mirror, or the surface of water), the only way to predict the ratio of reflection:pass-through is by relying on probability. The question I have is, why is it not also (at least partially) reliant on the accuracy with which we can shoot the photons to exactly the same spot? Can we *ONLY* refer to the probability calculations to determine reflection percentage, or is our inability to have 100% perfect photon-firing accuracy also part of the equation?

Let me explain. Let's say we have a semi-reflective piece of glass that reflects 4% of photons that we shoot at it. Let's say we point our "photon-gun" at one *particular* point, and do not move it, nor the glass, in any way. Also assume that we fire exactly one photon at a time. From what I understand, physicists posit that, even though we fire the photons in this manner, roughly only 4% will be reflected.

This is all well and good, **but**, what I want to know is if our capability for *accuracy* is high enough that this level of reflection doesn't occur because of aiming inaccuracies. Are we *actually* 100% certain that the atoms of the photon emitter AND the glass do not move, shudder, vibrate, or otherwise change position enough to change the accuracy of our photon path? How can we be certain that this level of reflection is *not* caused by chance pass-through's based on movement of any of the objects involved? I'm not disputing that the established theories are true, I'm just wondering how we have determined that not to be the case.

Could the movement (even, say, the vibration, or "jiggling") of the atoms be responsible for light passing through or not passing through? Photons move so quickly that I could imagine a photon " dodging " a single atom simply by virtue of the atom's ever-so-slight movement.

Thanks in advance, this is really bugging me and it's making it hard to move on with understanding the physics behind light. I'll try to explain further in the comments if I did not make my question clear.</selftext>
</submission>
