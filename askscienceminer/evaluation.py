__author__ = 'bert'

#
#
# The following script is a example run of my program. Each run will produce one
# matplotlib plot showing the evaluation of several methods of document classification.
# Furthermore it should be the starting point for your evaluation of my program.
# The corpus is located in the corpusxml folder.
#
# You need the following python libraries to run the program:
# NLTK (obviously)
# PRAW: https://praw.readthedocs.org/en/stable/
# sklearn: http://scikit-learn.org/stable/install.html
# matplotlib: http://matplotlib.org/
#
# Instructions for running from python console:
#   import evaluation
#   evaluation.full_run()
#
#
#
from scraping import scraper as scp
from mining import classifier as cls

def full_run():
    """
    Full run to test complete functionality of scraping and classification
    :return:
    """
    scraper = scp.CorpusSubmission()
    scraper.build_corpus()
    eval = cls.ClassificationEvaluator(cached=False,feature_vector_size=1000)
    results = eval.run_eval(eval.test_runs)
    eval.print_results(results)

def fast_run():
    """
    Fast run with scraping already done and cached data.
    You can play around with ClassificationEvaluator.test_runs in mining/classifier.py for to check out different parameters.
    The corpus size is around 11.700 submissions so 10.000 for the train_set and 1000 for the test_set should be the maximum.
    Big feature vectors(>2000) will need LOTS of RAM.
    Just use the cached version so it is faster, because corpus data doesn't need to be preprocessed each time.

    :return:
    """
    eval = cls.ClassificationEvaluator(cached=True,feature_vector_size=1000)
    results = eval.run_eval(eval.test_runs)
    eval.print_results(results)

def report_run1():
    """
    Run used to get first part of results presented in the report
    :return:
    """
    eval1 = cls.ClassificationEvaluator(cached=True,feature_vector_size=1000)
    results1 = eval1.run_eval(eval1.test_runs)
    eval1.print_results(results1)

def report_run2():
    """
    Run used to get second part of results presented in the report
    :return:
    """
    eval2 = cls.ClassificationEvaluator(cached=True,feature_vector_size=2000)
    results2 = eval2.run_eval(eval2.test_runs)
    eval2.print_results(results2)