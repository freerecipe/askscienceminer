__author__ = 'bert'

import random
import nltk
import cPickle
import codecs

from nltk.classify import apply_features
from nltk.classify import SklearnClassifier
from nltk.corpus import stopwords
from nltk.text import TextCollection

from sklearn.svm import LinearSVC
from sklearn.linear_model import SGDClassifier
from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import Perceptron

from time import time
import matplotlib.pyplot as plt
import numpy as np

from scraping import AskScienceXMLCorpusReader


class ClassificationEvaluator:
    'Class for evaluating classifiers for the askscience reddit corpus'

    def __init__(self, cached=False, feature_vector_size=2000):
        """
        Constructor. Prepares and normalizes corpus data and classifiers.
        :param cached: use prepared (pickled) data, speeds up classification, run once to cache
         if fetching a new corpus, use cached=True afterwards
        :param feature_vector_size: the size of the feature vector
        """
        self.custom_stopwords = [u'i', u'is', u'if', u've', u's', u'would', u'why', u'what', u'how', u'the', u'a',
                                 u'or', u'and', u'but']
        self.ask_science = AskScienceXMLCorpusReader.AskScienceXMLCorpusReader()

        if cached:
            self.docs = cPickle.load(codecs.open('docs.pck', 'r'))

            random.shuffle(self.docs)
            self.text_collection = TextCollection([text[0] for text in self.docs])

            words = cPickle.load(codecs.open('words.pck', 'r'))
            stop_words = frozenset(self.custom_stopwords + list(stopwords.words('english')))
            normalized_words = [w.lower() for w in words if w.isalpha()]

            freq_dist = cPickle.load(codecs.open('freq_dist.pck', 'r'))
        else:
            self.docs = self.create_docs()
            cPickle.dump(self.docs, codecs.open('docs.pck', 'w'))

            random.shuffle(self.docs)
            self.text_collection = TextCollection([text[0] for text in self.docs])

            words = self.ask_science.words(only_titles=False)
            cPickle.dump(words, codecs.open('words.pck', 'w'))
            stop_words = frozenset(self.custom_stopwords + list(stopwords.words('english')))
            normalized_words = [w.lower() for w in words if w.isalpha()]

            freq_dist = nltk.FreqDist(w for w in normalized_words if w not in stop_words)
            cPickle.dump(freq_dist, codecs.open('freq_dist.pck', 'w'))

        # freq_dist.plot(100)
        self.feature_vector_size = feature_vector_size
        offset = 0
        self.word_features = freq_dist.keys()[offset:feature_vector_size + offset]

        self.nb_cls = nb_cls = nltk.NaiveBayesClassifier
        self.mult_nb_cls = mult_nb_cls = SklearnClassifier(MultinomialNB(alpha=.01))
        self.linSVM_cls = linSVM_cls = SklearnClassifier(LinearSVC())
        self.sgd_cls = sgd_cls = SklearnClassifier(SGDClassifier(n_jobs=2))
        self.perceptron_cls = perceptron_cls = SklearnClassifier(Perceptron(n_iter=50))

        binary_features = self.binary_features
        tf_features = self.tf_features
        tf_idf_features = self.tf_idf_features

        self.test_runs = [

            {'name': 'multinomial Naive Bayes', 'classifier': mult_nb_cls, 'train_set_size': 10000,
             'test_set_size': 1000, 'feature_function': tf_idf_features},
            {'name': 'linear SVM', 'classifier': linSVM_cls, 'train_set_size': 10000, 'test_set_size': 1000,
             'feature_function': tf_idf_features},
            {'name': 'SGD', 'classifier': sgd_cls, 'train_set_size': 10000, 'test_set_size': 1000,
             'feature_function': tf_idf_features},
            {'name': 'Perceptron', 'classifier': perceptron_cls, 'train_set_size': 10000, 'test_set_size': 1000,
             'feature_function': tf_idf_features},

            {'name': 'multinomial Naive Bayes', 'classifier': mult_nb_cls, 'train_set_size': 10000,
             'test_set_size': 1000, 'feature_function': binary_features},
            {'name': 'linear SVM', 'classifier': linSVM_cls, 'train_set_size': 10000, 'test_set_size': 1000,
             'feature_function': binary_features},
            {'name': 'SGD', 'classifier': sgd_cls, 'train_set_size': 10000, 'test_set_size': 1000,
             'feature_function': binary_features},
            {'name': 'Perceptron', 'classifier': perceptron_cls, 'train_set_size': 10000, 'test_set_size': 1000,
             'feature_function': binary_features},
        ]

    def create_docs(self):
        """
        Creates/prepares documents for classification. A askscience submission is chunked in words, words converted to
        lower case and everything but alphabetic strings removed. The list of words is then combined with its
        category in a tupel.

        :return:[([word,word,word,...],cat),([word,word,word,...],cat),...]
        """
        ask_science = self.ask_science
        docs = [([w.lower() for w in list(ask_science.words(fileid)) if w.isalpha()], category)
                for category in ask_science.categories()
                for fileid in ask_science.fileids(category)]
        return docs

    def binary_features(self, document):
        """
        Extracts a binary feature vector for a document.
        Binary bag of words.
        :param document: the list of words to apply the feature function on
        :return:a dictionary: key = 'contains(word)', value = value of contains(word)
        """
        word_features = self.word_features
        document_words = set(document)
        features = {}
        for word in word_features:
            features['contains(%s)' % word] = (word in document_words)
        return features

    def tf_features(self, document):
        """
        Extracts a term frequency feature vector for a document.
        Bag of words with term frequency
        :param document: the list of words to apply the feature function on
        :return: a dictionary: 'tf(word)', value =  value of tf(word)
        """
        word_features = self.word_features
        tf = self._tf
        features = {}
        maxi = 0
        for word in word_features:
            tfr = tf(document, word)
            features['tf(%s)' % word] = tfr
            if tfr > maxi:
                maxi = tfr
        if maxi > 0:
            for value in features.itervalues():
                value /= maxi
        return features

    def _tf(self,document, word):
        """
        Method for counting word frequencies in a document
        :param document: the document
        :param word: the word to be counted
        :return: count of word in document
        """
        tf = 0
        for w in document:
            if w == word:
                tf += 1
        return tf

    def tf_idf_features(self, document):
        """
        Extracts a tf_idf feature vector for a document.
        Bag of words with tf_idf
        Method for counting word frequencies in a document
        :param document: the list of words to apply the feature function on
        :return: a dictionary: key = 'tf_idf(word)', value =  value of tf_idf(word)
        """
        word_features = self.word_features
        text_collection = self.text_collection
        features = {}
        for word in word_features:
            tfidf = text_collection.tf_idf(word, document)
            features['tf_idf(%s)' % word] = tfidf
        return features

    def run_classification(classifier, train_set, test_set):
        """
        Trains a classifier with train_set and then evaluates accuracy with
        test_set.
        :param classifier: the classifier to be used
        :param train_set: the set for training
        :param test_set: the set for evaluation
        :return:
        """
        classifier = classifier.train(train_set)
        acc = nltk.classify.accuracy(classifier, test_set)
        return acc


    def run_eval(self, test_runs):
        """
        Runs a series of test_runs in order to evaluate performance and accuracy of different classifiers and parameters
        :param test_runs: A list of test_run configurations, see self.test_runs for format
        :return: the results of the evaluation, containing description of the test run, the run times for training and testing
        and the accuracy
        """
        results = []
        current_feature_function = None
        train_set = None
        test_set = None
        for run in test_runs:
            name = run['name']
            classifier = run['classifier']
            train_set_size = run['train_set_size']
            test_set_size = run['test_set_size']
            feature_function = run['feature_function']

            # word_features = freq_dist.keys()[0:feature_vector_size]

            if feature_function != current_feature_function:
                # lazy evaluation is super slow for multiple test runs, distorts time measurements
                # train_set = apply_features(feature_function, self.docs[:train_set_size])
                # test_set = apply_features(feature_function, self.docs[-test_set_size:])
                current_feature_function = feature_function
                featuresets = [(feature_function(n), g) for (n, g) in self.docs]

            train_set = featuresets[:train_set_size]
            test_set = featuresets[-test_set_size:]

            description = name + '\n' + '/' + '#train:' + str(train_set_size) + '/' + '#test:' + str(
                test_set_size) + '/' + '#fv:' + str(self.feature_vector_size) + '\n/' + feature_function.__name__

            print 'Running ' + description + '\n'

            t0 = time()
            classifier = classifier.train(train_set)
            training_time = time() - t0

            t0 = time()
            acc = nltk.classify.accuracy(classifier, test_set)
            test_time = time() - t0

            result = {'name': description, 'accuracy': acc, 'training_time': training_time, 'test_time': test_time}
            results.append(result)
            print result
            print '=' * 80
        return results

    def print_results(self, results):
        """
        Method for plotting test results from run_eval.
        Adapted from http://scikit-learn.org/stable/_downloads/document_classification_20newsgroups.py .
        :param results: test results produced by run_eval
        """
        plot_data = [[], [], [], []]
        for result in results:
            plot_data[0].append(result['name'])
            plot_data[1].append(result['accuracy'])
            plot_data[2].append(result['training_time'])
            plot_data[3].append(result['test_time'])

        indices = np.arange(len(results))
        clf_names, score, training_time, test_time = plot_data
        t_max = max(np.max(training_time), np.max(test_time))
        training_time = np.array(training_time)  # / t_max *10
        test_time = np.array(test_time)   / t_max *10
        score = np.array(score) * 100

        #plt.figure(figsize=(12, 8))
        plt.figure(figsize=(14, 10))
        plt.title("Score")
        plt.barh(indices, score, .2, label="score", color='r')
        plt.barh(indices + .3, training_time, .2, label="training time", color='g')
        plt.barh(indices + .6, test_time, .2, label="test time", color='b')
        plt.yticks(())
        plt.legend(loc='best')
        plt.subplots_adjust(left=.25)
        plt.subplots_adjust(top=.95)
        plt.subplots_adjust(bottom=.05)
        for i, c in zip(indices, clf_names):
            #plt.text(-.3 * 100, i, c)
            plt.text(-t_max * .35, i, c)
        plt.show()
