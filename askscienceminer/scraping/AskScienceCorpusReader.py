__author__ = 'bert'

from nltk.corpus.reader.plaintext import CategorizedPlaintextCorpusReader
from nltk.corpus.reader.util import *


class AskScienceCorpusReader(CategorizedPlaintextCorpusReader):
    'a NLTK corpus reader for the AskScience corpus'

    def __init__(self, root, *args, **kwargs):
        CategorizedPlaintextCorpusReader.__init__(self,
                                                  root,
                                                  fileids=r'(?!\.).*\.txt',
                                                  cat_pattern=r'(astro|bio|chem|computing|eng|geo|maths|med|neuro|physics|psych|soc)/.*')

    def titles(self, fileids=None, categories=None):
        if fileids == None:
            if categories == None:
                fileids = self.fileids()
            else:
                fileids = self.fileids(categories)
        cv = concat([self.CorpusView(path, self._read_title_block, encoding=enc)
                     for (path, enc, fileid)
                     in self.abspaths(fileids, True, True)])
        #self._read_para_block()
        print type(cv)
        return cv
        # return [self.paras(x)[0] for x in fileids]

    def _read_title_block(self,stream):
        titles = []
        for title in read_regexp_block(stream,start_re=r'##', end_re=r'\t'):
            print 'title:', title
            titles.append([self._word_tokenizer.tokenize(title)
                          for sent in self._sent_tokenizer.tokenize(title)])
        return titles
        #return [self._read_para_block(stream)[0]]

# Testing
ascr = AskScienceCorpusReader(root='corpus')
ascr.fileids()
print [x for x in ascr.titles(ascr.fileids()[:20])]  # len(ascr.titles(categories=['physics','soc']))
