__author__ = 'bert'

from nltk.corpus.reader import CategorizedCorpusReader, XMLCorpusReader
from nltk.tokenize import WordPunctTokenizer
import settings


class CategorizedXMLCorpusReader(CategorizedCorpusReader, XMLCorpusReader):
    """
    A corpus reader for corpora consisting out of xml documents that are categorized into folders.
    This class is adapted from code presented at http://stackoverflow.com/a/6915467
    """

    def __init__(self,*args, **kwargs):
        CategorizedCorpusReader.__init__(self,kwargs)
        XMLCorpusReader.__init__(self,wrap_etree=False, *args, **kwargs)
        #print 'cons ', self.fileids()

    def _resolve(self, fileids, categories):
        fileid_list = []
        if fileids is not None and categories is not None:
            raise ValueError('Specify fileids or categories, not both')
        if categories is not None:
            return self.fileids(categories)
        elif fileids is not None:
            fileid_list.append(fileids)
            return fileid_list
        else:
            return self.fileids()

        # All of the following methods call the corresponding function in ChunkedCorpusReader
        # with the value returned from _resolve(). We'll start with the plain text methods.
    def raw(self, fileids=None, categories=None):
        return XMLCorpusReader.raw(self, self._resolve(fileids, categories))

    def words(self, fileids=None, categories=None, only_titles=False):
        words = []
        #fileids = self._resolve(fileids, categories)
        #print 'debug: ' + str(fileids)
        word_tokenizer=WordPunctTokenizer()
        if only_titles==True:
            text = ' '.join(self.title(fileids, categories))
        else:
            text = self.text(fileids, categories)

        return word_tokenizer.tokenize(unicode(text))

    # This returns a string of the text of the XML docs without any markup
    def text(self, fileids=None, categories=None):
        fileids = self._resolve(fileids, categories)
        #print 'debug: ' + str(fileids)
        text = ""
        for fileid in fileids:
            for i in self.xml(fileid).getiterator():
                if i.text:
                    text += ' ' + i.text
        return text

    def title(self, fileids=None, categories=None):
        return self.fieldtext('title',fileids,categories)


    # This returns all text for a specified xml field
    def fieldtext(self, field, fileids=None, categories=None):
        fileids = self._resolve(fileids,categories)
        #elts = []
        texts = []
        for fileid in fileids:
            elt = XMLCorpusReader.xml(self,fileid)
            text = elt.find(field).text
            #print fileid + ':' +  str(elt)
            #elts.append(elt)
            texts.append(text)
        return texts

class AskScienceXMLCorpusReader(CategorizedXMLCorpusReader):
    """
    Adapted version of CategorizedXMLCorpusReader for the askscience reddit corpus
    """
    def __init__(self):
        CategorizedXMLCorpusReader.__init__(self,root=settings.PROJECT_ROOT + '/corpusxml',fileids=r'(?!\.).*\.xml',
                                            cat_pattern=r'(astro|bio|chem|computing|eng|geo|maths|med|neuro|physics|psych|soc)/.*')


#lots of testing
#reader = CategorizedCorpusReader(cat_pattern=r'/w')
#reader = AskScienceXMLCorpusReader()
#print reader
#print reader.fileids(categories='geo')
#reader.raw()
#print reader
#print reader.fileids()
#print reader.categories()
#print reader.words()
#print [x for x in reader.raw(reader.fileids()[:20])]
#print [x for x in reader.categories()]
#elts = reader.fieldtext(fileids=reader.fileids(categories='geo'))
#paras = reader.paras(fileids=reader.fileids(categories='geo'))
#print elts
#print elts[0:5]
#print type(elts[0])
#print elts[0]
#print elts[0].find('title').text
#print list(elts[0])
#print paras[0:5]
#texts = reader.fieldtext('selftext', fileids=reader.fileids(categories='geo'))
#print texts
#print reader.title(categories='geo')
#print reader.sents(fileids=['geo/t3_2x3d5l.xml'])
#print reader.sents(categories='geo', only_titles=True)
#print reader.sents(fileids=['geo/t3_2xpweq.xml'], only_titles=True)
#t3_2xpweq
#print reader.words2(categories='geo')
#print set(reader.words(categories='geo')) & set(reader.words2(categories='geo'))
#w = reader.words(categories='geo')
#w2 = reader.words2(categories='geo')
#print len(w) , len(w), len(set(w).intersection(w))
#print set(w).intersection(w)
#print [x for x in reader.words(reader.fileids()[220:230])]
#print [x for x in reader.words(reader.fileids()[220:230],only_titles=True)]
#print settings.PROJECT_ROOT
#print reader._resolve(fileids='astro/t3_32la0w.xml', categories=None)
#print reader.words('astro/t3_32la0w.xml', only_titles=True)
#print reader.words()