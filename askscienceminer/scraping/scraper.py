__author__ = 'bert'

import praw
import os
import codecs
from lxml import etree as ET

user_agent = 'python2:/r/askscienceminer:v10.0'
subreddit = 'askscience'
categories = ['astro', 'bio', 'chem', 'computing', 'eng', 'geo', 'maths', 'med', 'neuro', 'physics', 'psych', 'soc']
title_tag = '##'


class CorpusSubmission():
    'AskScience submission for storing in AskScience corpus'

    def __init__(self, fullname=None, category=None, title=None, selftext=None, submission=None):
        if submission == None:
            self.fullname = fullname
            self.category = category
            self.title = title
            self.selftext = selftext
        else:
            assert isinstance(submission, praw.objects.Submission)
            self.fullname = submission.fullname
            self.category = self.__getCategoryFromSubmission(submission)
            if self.category == None:
                self.category = 'none'
            self.title = submission.title
            self.selftext = submission.selftext

    def __str__(self):
        return self.fullname

    def __unicode__(self):
        return unicode(self.fullname)

    def __repr__(self):
        return self.fullname

    def __getCategoryFromSubmission(self, submission):
        flair = submission.link_flair_css_class
        if flair != None:
            return flair.split()[-1]
        return None

    def storeToTextFile(self):
        """
        DEPRECATED DON'T USE NOT TESTED WELL, USE storeToXmlFile
        Stores askscience reddit subbmission to a .txt file,
        each category in it's own folder.
        :return: the 'fullname' (the reddit submission identifier, which is unique) of the submission for debugging
        """
        if self.category in categories:
            dir = os.getcwd() + "/" + "corpus/" + self.category + "/"
            if not os.path.exists(dir):
                os.makedirs(dir)
            filename = dir + self.fullname + '.txt'
            with codecs.open(filename, 'w', encoding='utf8') as f:
                text = title_tag + self.title.replace('\t', '    ') + title_tag + '\n'
                if self.selftext != '':
                    # text += "\n\n\t" + self.selftext.replace('\t','    ')
                    for line in self.selftext.splitlines(True):
                        text += '\t' + line
                f.write(text)
        return self.__str__()

    def storeToXMLFile(self):
        """
        Stores askscience reddit subbmission to a .xml file in an xml format,
        each category in its own folder.
        :return: the 'fullname' (the reddit submission identifier, which is unique) of the submission for debugging
        """
        if self.category in categories:
            dir = os.getcwd() + "/" + "corpusxml/" + self.category + "/"
            if not os.path.exists(dir):
                os.makedirs(dir)
            filename = dir + self.fullname + '.xml'
            # with codecs.open(filename,'w',encoding='utf8') as f:
            submission = ET.Element("submission", )
            ET.SubElement(submission, "fullname", name=self.fullname)
            ET.SubElement(submission, "category", name=self.category)
            ET.SubElement(submission, "title").text = self.title
            try:
                ET.SubElement(submission, "selftext").text = self.selftext
            except ValueError as ve:
                print self.fullname, ve

            tree = ET.ElementTree(submission)
            tree.write(filename, pretty_print=True, encoding='utf8')

        return self.__str__()

    def build_corpus(self):
        """
        Builds a corpus from ask reddit submissions.
        """
        reddit = praw.Reddit(user_agent=user_agent)
        # submissions = reddit.get_subreddit(subreddit).get_top_from_all(limit = 1000)
        for cat in categories:
            submissions = reddit.get_content(
                'http://www.reddit.com/r/askscience/search.json?q=flair%3A%27{0}%27&sort=new&restrict_sr=on'.format(
                    cat), limit=1000)
            corpus = []
            for x in submissions:
                corpus.append(CorpusSubmission(submission=x))
            # print(cat + ": " + str(len([str(sub.storeToTextFile()) for sub in corpus])))
            print(cat + ": " + str(len([str(sub.storeToXMLFile()) for sub in corpus])))

